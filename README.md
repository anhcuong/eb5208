# Introduction

# How to run

```bash

# Install dependencies
make install

# Download subzones from data.gov and rename it into subzones.json
wget https://data.gov.sg/dataset/2998fad4-ef95-483c-bdf8-a50437446681/download

# Convert subzone.kml to json format
python extract_data.py

# Get result from google map
python get_travel_time.py {GOOGLE_API} {Origin}

Eg: python get_travel_time.py {GOOGLE_API} 'Paya Lebar MRT'
```