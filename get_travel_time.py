import sys
import json
import math

import requests
import numpy as np


if len(sys.argv) < 3:
    print('Please input GOOGLE_MAP_API_KEY' + \
        'and Origin. Eg: python get_travel_time.py GOOGLE_MAP_API_KEY Origin')
else:
    api_key = sys.argv[1]
    origin = sys.argv[2]
    print 'Origin: {}'.format(origin)

    distance_api = 'https://maps.googleapis.com/maps/api/distancematrix/json?' + \
        'mode={mode}&origins={origin}&destinations={destinations}&key={api_key}'
    result_file = '{origin}_{zone}_{mode}'

    destinations = json.load(open('subzones.json'))
    MAX_ITEM_PER_GOOGLE_REQUEST = 25.0

    for mode in ['driving', 'transit']:
        for zone, dest_coordinates in destinations.iteritems():
            max_chunks = math.ceil(len(dest_coordinates) / MAX_ITEM_PER_GOOGLE_REQUEST)
            splited_coordinates = np.array_split(dest_coordinates, max_chunks)
            for coordinates in splited_coordinates:
                destinations = ''
                for coordinate in coordinates:
                    destinations += '{},{}|'.format(coordinate[1], coordinate[0])
                url = distance_api.format(
                    origin=origin,
                    destinations=destinations[:-1],
                    api_key=api_key,
                    mode=mode
                )
                response = requests.get(url).json()['rows'][0]['elements']
                with open(result_file.format(
                    origin=origin,
                    zone=zone,
                    mode=mode
                ), 'a') as f:
                    json.dump(response, f, indent=2)
