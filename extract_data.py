import json

from fastkml import kml


kml_file = 'subzones.kml'
result_file = 'subzones.json'
with open(kml_file, 'rt') as f:
    doc = f.read()


k = kml.KML()
k.from_string(doc)


folder = list(list(k.features())[0].features())
placemarks = list(folder[0].features())
result = {}
total_data_points = 0
for placemark in placemarks:
    name = placemark.name
    points = placemark._geometry.geometry._geoms[0]._exterior._geoms
    total_data_points = total_data_points + len(points)
    coordinates = [[point._coordinates[0], point._coordinates[1]] for point in points]
    result[name] = coordinates

with open(result_file, 'w') as f:
    json.dump(result, f, indent=2)


print('Total zones: {}'.format(len(result.keys())))
print('Total data points: {}'.format(total_data_points))
print('Completed...')
